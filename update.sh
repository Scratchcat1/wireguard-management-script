#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
PEER_FILE="$SCRIPT_DIR"/peers.txt
INTERFACE_PATH_PREFIX="/etc/wireguard/"
INTERFACE_PATH_SUFFIX="_conf_base.txt"

# fetch the latest copy of peers.txt
git pull

# generate the configs by merging the interface and peer details
file=$(sudo find "$INTERFACE_PATH_PREFIX" | grep "$INTERFACE_PATH_SUFFIX")
for file in "$file"; do
    interface=$(echo "$file" | tail -c +$(expr ${#INTERFACE_PATH_PREFIX} + 1) | head -c -$(expr ${#INTERFACE_PATH_SUFFIX} + 1))
    printf "\n\nUpdating interface $interface"

    sudo cat /etc/wireguard/${interface}_conf_base.txt | sudo tee /etc/wireguard/${interface}.conf > /dev/null
    cat "$PEER_FILE" | sudo tee --append /etc/wireguard/${interface}.conf
done