#!/bin/bash
# configuration
LISTENPORT=51820
KEY_DIR=.wg
LAN_ALLOW=0
LAN_INTERFACE=eth0
WG_INTERFACE=wg0
SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
PEER_FILE="$SCRIPT_DIR"/peers.txt
HOSTNAME=$(hostname)
ADDRESS=""  # Currently is required but is controlled by an optional parameter. Remove or move to generated
ENDPOINT=""

HELP_MESSAGE="Configure Wireguard script
Usage: configure_wireguard [OPTION]
Create a wireguard interface configuration (wg-quick) with many peers registered in a git repository.
Options:
    -a, --address=ADDRESS     Set wg interface address to be ADDRESS.
    -d, --directory=DIR       Use the directory DIR to store keys. Relative to user's home directory.
    -e, --endpoint=ADDRESS    Endpoint for the device.
    -h, --help                Display this help message.
    -L, --lan-allow           Allow access to LAN from remote devices
    -l, --lan-interface=NAME  Name the interface to put packets onto to access server's LAN.
    -H, --hostname=NAME       Label the Peer entry for this device as NAME.
    -p, --port=PORT           Port to listen on for connections.
    -w, --wg-interface=NAME   Name of the wireguard interface to configure"


# manage flags
while [ -n "$1" ]; do
    case "$1" in

    -a|--address)
        ADDRESS="$2"
        echo "Address set to $ADDRESS"
        shift
        ;;

    -d|--directory)
        KEY_DIR="$2"
        echo "Key directory set to $KEY_DIR"
        shift
        ;;
    
    -e|--endpoint)
        ENDPOINT="$2"
        echo "Endpoint set to $ENDPOINT"
        shift
        ;;

    -h|--help)
        echo "$HELP_MESSAGE"
        exit 0;;

    -L|--lan-allow)
        echo "Enabling access to LAN"
        LAN_ALLOW=1
        ;;

    -l|--lan-interface)
        LAN_INTERFACE="$2"
        echo "Lan interface set to $LAN_INTERFACE"
        shift
        ;;

    -H|--hostname)
        HOSTNAME="$2"
        echo "Label for this host set to $HOSTNAME"
        shift
        ;;

    -p|--port)
        LISTENPORT="$2"
        echo "Listen port set to $LISTENPORT"
        shift
        ;;

    -w|--wg-interface)
        WG_INTERFACE="$2"
        echo "Device set to $WG_INTERFACE"
        shift
        ;;

    --) shift
        break
        ;;

    *) echo "Unknown option $1"
       echo "$HELP_MESSAGE"
       exit 1
       ;;

    esac
    shift
done

# if [ -n "$1" ]; then
#     ENDPOINT="$1"
# else
#     echo "Error: Endpoint not specified. Try with ./configure_wireguard.sh [parameters] -- \"x.x.x.x:yyyy\" inserting the correct address"
#     exit 1
# fi



# setup the directory
echo "Setting up $KEY_DIR directory"
cd $HOME
mkdir "$KEY_DIR"
cd "$KEY_DIR"
umask 077
chmod go-rwx .

# generate the keys
echo "Generating wireguard keys"
wg genkey > privatekey
wg pubkey < privatekey > publickey

PRIVATEKEY="$(cat privatekey)"
PUBLICKEY="$(cat publickey)"
echo "Wireguard public key is $PUBLICKEY"

# setup the interface
echo "Setting up the interface"
printf "[Interface]
Address = $ADDRESS
ListenPort = $LISTENPORT
PrivateKey = $PRIVATEKEY
" | sudo tee /etc/wireguard/${WG_INTERFACE}_conf_base.txt > /dev/null

# if allowing access to LAN add forwarding to the LAN interface
if [ $LAN_ALLOW -eq 1 ]; then
    printf "
PostUp = iptables -A FORWARD -i %i -j ACCEPT; iptables -A FORWARD -o %i -j ACCEPT; iptables -t nat -A POSTROUTING -o $LAN_INTERFACE -j MASQUERADE
PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -D FORWARD -o %i -j ACCEPT; iptables -t nat -D POSTROUTING -o $LAN_INTERFACE -j MASQUERADE
" | sudo tee --append /etc/wireguard/${WG_INTERFACE}_conf_base.txt > /dev/null
fi

# add the current device as a new peer
echo "
#$HOSTNAME
[Peer]
PublicKey = $PUBLICKEY
AllowedIPs = $ADDRESS" >> $PEER_FILE

# add the endpoint if specified
if [ ! -z "$ENDPOINT" ]; then
    echo "Endpoint = $ENDPOINT" >> $PEER_FILE
fi 

# generate the config by merging the interface and peer details
sudo cat /etc/wireguard/${WG_INTERFACE}_conf_base.txt | sudo tee /etc/wireguard/${WG_INTERFACE}.conf > /dev/null
cat "$PEER_FILE" | sudo tee --append /etc/wireguard/${WG_INTERFACE}.conf

exit 0