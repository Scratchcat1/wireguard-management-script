# Wiregit - Wireguard management scripts

Setup and manage wireguard peers

# Why
Wireguard does not and [will not](https://www.wireguard.com/#conceptual-overview) handle key distribution.

> All issues of key distribution and pushed configurations are **out of scope** of WireGuard; these are issues much better left for other layers, lest we end up with the bloat of IKE or OpenVPN.

This leaves the user to manually copy keys between each device *each time* a new device is added. 

This project aims to simplify wireguard setup and key exchange using simple bash scripts and git.

# Features
- Quickly and easily add new devices to the network.
- Use git to determine when a public key was added and by whom.
- Optionally use protected branches to only allow authorised members to write to master.

# Usage
## Setup
1. Fork this repository by clicking [here](https://gitlab.com/Scratchcat1/wireguard-management-script/forks/new).
1. Make your new repository private. Although without writing to `peers.txt` others cannot access your network it's nice to be cautious. 
1. Clone your repository to all devices you wish to keep in sync.

## Management
### Adding a new peer
1. Clone the repository.
1. Run `setup.sh`. For example if the device should have a wireguard IP of *192.168.100.2* and has an external IP of *1.2.3.4* run `./setup.sh -a 192.168.100.2 -- 1.2.3.4:51820`.

### Updating peers
1. Run `./update.sh`.

The updated copy of `peers.txt` will be pulled and integrated into the wireguard config.

Automate this by adding a cron task to automatically pull in new peers.

# Todo
- Generate next address automatically.
- Add cron task to periodically refresh list of peers.
- Add branch creation as an option.
- ~~Make the endpoint optional.~~