#!/bin/bash
DATE=$(date)
SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"

cd "$SCRIPT_DIR"
git pull

./configure_wireguard.sh $@
if [ $? -ne 0 ]; then # $? returns the exit status of the previous command
    echo "Wireguard configuration script failed"
    exit 1
fi

git add peers.txt
git commit -m "Configured new peer - $DATE"
git push